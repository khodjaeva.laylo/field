import React from 'react';
import Navbar from './components/Navbar';
import './App.css';
import Asosiy from './components/pages/Asosiy';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Footer from './components/Footer'
import Blogs from './components/pages/Blogs';
import Kurslar from './components/pages/Kurslar';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component={Asosiy} />
          <Route path='/kurslar' component={Kurslar} />
          <Route path='/Blogs' component={Blogs} />
        </Switch>
        <Footer />
      </Router>
    </>
  );
}

export default App;
