import React from 'react';
import './Cards.css';
import CardItem from './CardItem';
import {Container, Col, Row} from 'reactstrap';

function Cards() {
  return (
    <>
    
        
    <div className='cards'>
    <Container className=" mt-5" >
        <h1 className="text-center mt-5 pt-5">Biz haqimizda</h1>
        <p>Work Field jamoasi hozirda marketing sohasida ishlamoqda. Uzbekistandagi milliy marketing jamoa. Hozirda ishsiz yurgan mutaxasislarni ish bilan ta'minlamoqda</p>
        <h3>Work Field Marketing Agency xizmatlari:</h3>
      <ul>
        <li> Dizayner konsultatsiyasi va xizmatlari.</li>
        <li>Naming (Nomlash) xizmati.</li>
        <li>Logotip yaratish.</li>
        <li>Brend tanlash.</li>
        <li> Web saytlar yaratib berish.</li>
        <li> Anrdoid(komp) dasturlar yaratib berish.</li>
        <li>Strategiya tuzish</li>
      </ul>  
      </Container>
      <Container>
        <h3>Work Field Academy haqida</h3>
        <p>Bugungi kunda sifatli ta'lim hamda malakali mutahassislarga ehtiyoj yuqori lekin tayyorlovchi tashkilotlar kam.

Work Field Akademiyasining asosiy maqsadi esa ana shunday talablarga javob bera oladigan malakali  mutahassilarni tayyorlashga qaratilgan.
Akademiya bugungi kunda IT sohasi mutahassislari tayyorlash uchun sifatli ta'lim dasturlarini va texnologik platformani yaratmoqda.
</p> 
      <h4>Work Field quyidagi sohalar bo'yicha sifatli ta'lim olishingizni kafolatlaydi:</h4>
      <ul>
        <li>Photoshop</li>
        <li> Illustrator</li>
        <li>3D Max (maya)</li>
        <li>Logo Brending</li>
        <li> SMM</li>
        <li>Kompyuter savodxonligi</li>
        <li>  Dasturlash tillari  (Python, Java C++ va boshqa )</li>
        <li>AutoCad</li>
      </ul>  
      </Container>
      <h1> Grafik dizayn kursidan parchalar</h1>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
            <CardItem
              src='images/card1.jpg'
              text='Suratda korib turgan ish amallari grafik dizayn kursidan parcha qilinib, sizlarga taqdim etildi. Sizlarga ham foydasi tegsa biz juda ham xursand bolamiz'
              label='Adrenaline'
              path='/kurslar'
            />
            <CardItem
              src='images/card2.jpg'
              text='Suratda korib turgan ish amallari grafik dizayn kursidan parcha qilinib, sizlarga taqdim etildi. Sizlarga ham foydasi tegsa biz juda ham xursand bolamiz'
              label='Adrenaline'
              path='/kurslar'
            />
          </ul>
          <ul className='cards__items'>
            <CardItem
              src='images/card3.jpg'
              text='Suratda korib turgan ish amallari grafik dizayn kursidan parcha qilinib, sizlarga taqdim etildi. Sizlarga ham foydasi tegsa biz juda ham xursand bolamiz'
              label='Adrenaline'
              path='/kurslar'
            />
            <CardItem
              src='images/card4.jpg'
              text='Suratda korib turgan ish amallari grafik dizayn kursidan parcha qilinib, sizlarga taqdim etildi. Sizlarga ham foydasi tegsa biz juda ham xursand bolamiz'
              label='Adrenaline'
              path='/kurslar'
            />
            <CardItem
              src='images/card5.jpg'
              text='Suratda korib turgan ish amallari grafik dizayn kursidan parcha qilinib, sizlarga taqdim etildi. Sizlarga ham foydasi tegsa biz juda ham xursand bolamiz'
              label='Adrenaline'
              path='/kurslar'
            />
          </ul>
        </div>
      </div>
    </div>
    </>
  );
}

export default Cards;
