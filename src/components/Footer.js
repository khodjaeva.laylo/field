import React from 'react'; 
import './Footer.css';
import { Link } from 'react-router-dom';
import {Row, Col,Container} from 'reactstrap'
function Footer() {
  return (
    <div className='footer-container'>
      <section className='footer-subscription'>
        <div className='input-areas'>
        </div>
      </section>
      
     <Container className='footer-sub'>
     <Row>
       <Col>
       <p className='footer-subscription-head'>
         Biz bilan bog'lanish:
        </p>
     <Row>
       <Col sm="0"><i class="fas fa-phone-square-alt"></i></Col>
          <Col sm="6"> <p >Tel raqam: +998 9999-88-472 </p></Col>
       </Row>
       <Row>
       <Col md="0"> <i class="fas fa-map-marker-alt"></i></Col>
          <Col md="3">  <p >Toshkent shahri. </p></Col>
       </Row>
       </Col>
       <Col>
       <Container  >
        
        <h3>Work Field Akademiyasining afzalliklari:</h3>
      <ul className="ml-4">
        <li>Birinchi darslar mutlaqo bepul.</li>
        <li>Tajribali mutahassislardan ta'lim olish.</li>
        <li>Akademiya yaxshi natija ko'rsata olgan kadrlarni o'z jamoasiga qabul qiladi.</li>
      </ul>  
      </Container>
       </Col>
     </Row>
     </Container>
     <section class='social-media'>
        <div class='social-media-wrap'>
          <div class='footer-logo'>
            <Link to='/' className='social-logo'>
               FIELD
               <i class="fab fa-500px"></i>
            </Link>
          </div>
          <small class='website-rights'>FIELD © 2021</small>
          <div class='social-icons'>
            <Link
              class='social-icon-link facebook'
              to='/'
              target='_blank'
              aria-label='Facebook'
            > 
            
            <i class="fas fa-paper-plane"></i>
            </Link>
            <Link
              class='social-icon-link instagram'
              to='/'
              target='_blank'
              aria-label='Instagram'
            >
              <i class='fab fa-instagram' />
            </Link>
            <Link
              class='social-icon-link youtube'
              to='/'
              target='_blank'
              aria-label='Youtube'
            >
              <i class='fab fa-youtube' />
            </Link>
            <Link
              class='social-icon-link twitter'
              to='/'
              target='_blank'
              aria-label='Twitter'
            >
              <i class='fab fa-twitter' />
            </Link>
            <Link
              class='social-icon-link twitter'
              to='/'
              target='_blank'
              aria-label='LinkedIn'
            >
              <i class='fab fa-linkedin' />
            </Link>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Footer;


